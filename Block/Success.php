<?php


namespace Kowal\ZaufaneOpinieCeneo\Block;

class Success extends \Magento\Framework\View\Element\Template
{
    protected $_scopeConfig;
    protected $_order;
    protected $_checkoutSession;
    protected $_orderFactory;
    protected $_session;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Session\SessionManagerInterface $session,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->_scopeConfig = $scopeConfig;
        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        $this->_session = $session;
    }

    /**
     * Odczyt zmiennej dotyczącej klucza podanego przez Zaufane Opinie Ceneo
     * @return mixed
     */
    public function getAccountGuid()
    {
        return $this->_scopeConfig->getValue('zaufane_opinie/parametry/account_guid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Odczyt zmiennej decydującej o tym czy przekazywać do Zaufanych Opini Ceneo wszystkich klientów sklepu czy tylko tych przekierowanych z Ceneo
     *
     * @return mixed
     */
    public function czyPytacTylkoCeneo()
    {
        return $this->_scopeConfig->getValue('zaufane_opinie/parametry/tylko_ceneo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Odczyt zmiennej definującej ile dni po zakupie przesłać zapytanie o opinię
     * @return mixed
     */
    public function getDaysToRequest()
    {
        return $this->_scopeConfig->getValue('zaufane_opinie/parametry/ceneo_work_days_to_send_questionnaire', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    /**
     * Odczyt numeru zamówienia
     * @return mixed
     */
    public function getRealOrderId()
    {
        return $this->_checkoutSession->getLastOrderId();

    }

    /**
     * Odczyt pełnego numeru zamówienia
     * @return mixed
     */
    public function getIncrementOrderId()
    {
        $order = $this->_orderFactory->create()->loadByIncrementId($this->_checkoutSession->getLastRealOrderId());
        return $order->getIncrementId();

    }

    /**
     * Odczyt listy produktów
     * @return string
     */
    public function getOrderSkus()
    {
        $skus = '#';
        if ($this->_checkoutSession->getLastRealOrderId()) {
            $order = $this->_orderFactory->create()->loadByIncrementId($this->_checkoutSession->getLastRealOrderId());
            $items = $order->getAllItems();

            foreach ($items as $item) {
                $skus .= $item->getSku() . '#';
            }
        } else {
            $skus = '##';
        }
        return $skus;
    }

    /**
     * Odczyt adresu e-mail klienta
     * @return mixed
     */
    public function getEmail()
    {
        $order = $this->_orderFactory->create()->loadByIncrementId($this->_checkoutSession->getLastRealOrderId());
        return $order->getCustomerEmail();
    }

    /**
     * Sprawdzenie czy klient pochodzi od Ceneo
     * @return bool
     */
    public function isCeneoReferer()
    {

        $this->_session->start();
        if ($reffIsCeneo = $this->_session->getCeneoRefferer()) {
            $this->_session->unsCeneoRefferer();
            return true;
        } else {
            if ($this->czyPytacTylkoCeneo()) {
                return false;
            } else {
                return true;
            }
        }
    }
}
