# Zaufane opinie CENEO dla Magento 2

## Instalacja za pomocą composera

1. Zaloguj się do konsoli SSH
2. Przejdź do głównego folderu aplikacji Magento
3. Wyłącz cache oraz przełącz sklep w tryb developerski:

`php bin/magento cache:disable;`

`php bin/magento deploy:mode:set developer;`

4. Wykonaj polecenie:

`composer require kowal/module-zaufaneopinieceneo`

5. Uaktualnij Magento:

`php bin/magento setup:upgrade;`

`php bin/magento cache:flush;`


6. Zaloguj się do panelu administratora i skonfiguruj moduł

    a. Przejdź do: Sklep -> Konfiguracja -> Ceneo
    b. Wypełnij formularz
    
7. Włącz cache:

`php bin/magento cache:enable;`

8. Włącz sklep Magento w tryb produkcyjny

`php bin/magento deploy:mode:set production;`