<?php


namespace Kowal\ZaufaneOpinieCeneo\Observer\Frontend\Controller;

class ActionPredispatch implements \Magento\Framework\Event\ObserverInterface
{

    protected $_session;


    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $session
    )
    {
        $this->_session = $session;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {

        $this->setCeneoRefferer();

    }

    /**
     * Zapisanie do sessi informacji o tym czy klient jest przekierowany z Ceneo
     */
    public function setCeneoRefferer()
    {
        $httpReferer = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : null;
        if (strstr($httpReferer, "ceneo.pl") !== FALSE) {
            $this->_session->start();
            $this->_session->setCeneoRefferer(1);
        }
    }
}
